﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Singular.Web.MaintenanceHelpers;
using Singular.Web.Security;
using System.ComponentModel;
using Singular.Web.Data;
using OETLib;
using OETLib.Security;
using OETLib.Appointments;
using OETLib.SalesPeople;
using Singular.Web;

namespace OETWeb.Appointments
{
  public partial class Appointments : OETPageBase<AppointmentsVM>
  {
  }

  public class AppointmentsVM : OETStatelessViewModel<AppointmentsVM>
  {
    public Appointment Appointments { get; set; }

    public AppointmentList AppointmentList { get; set; }

    public Appointment EditingAppointment { get; set; }

    public Appointment ReschedulingAppointment { get; set; }

    protected override void Setup()
    {
      base.Setup();
      CommonData.Lists.Refresh(typeof(OETLib.Maintenance.ROAppointmentStatusList));
      this.AppointmentList = AppointmentList.GetAppointmentList();
      this.ValidationDisplayMode = ValidationDisplayMode.Controls | ValidationDisplayMode.SubmitMessage;
      this.EditingAppointment = null;
    }

    [WebCallable]
    public static Appointment GetAppointment(int AppointmentID)
    {
      return AppointmentList.GetAppointmentList().GetItem(AppointmentID);
    }

    public static Boolean checkSalesPersonSchedule(int SalesPersonProductID, DateTime AppointmentStartDateTime, DateTime AppointmentEndDateTime)
    {
      AppointmentList Appointments = AppointmentList.GetAppointmentList();
      foreach (Appointment item in Appointments)
      {
        bool StartsAfterAppointmentAndEndsDuringAppointmet = (AppointmentStartDateTime >= item.StartDate) && (AppointmentEndDateTime <= item.EndDate) 
          && !(AppointmentStartDateTime >= item.EndDate) && !(AppointmentEndDateTime <= item.StartDate);
        bool StartsBeforeAppointmentAndEndsAfterAppointmet = (AppointmentStartDateTime <= item.StartDate) && (AppointmentEndDateTime >= item.EndDate);
        bool StartsAfterAppointmentAndEndsAfterAppointmet = (AppointmentStartDateTime >= item.StartDate) && (AppointmentEndDateTime >= item.EndDate) && !(AppointmentStartDateTime >= item.EndDate);
        bool StartsBeforeAppointmentAndEndsBeforeAppointmet = (AppointmentStartDateTime <= item.StartDate) && (AppointmentEndDateTime <= item.EndDate) && !(AppointmentEndDateTime <= item.StartDate);

        if (StartsAfterAppointmentAndEndsDuringAppointmet || StartsBeforeAppointmentAndEndsAfterAppointmet || StartsAfterAppointmentAndEndsAfterAppointmet || StartsBeforeAppointmentAndEndsBeforeAppointmet)
        {
          SalesPeopleProduct itemSalesPersonProduct = SalesPeopleProductList.GetSalesPeopleProductList().GetItem(item.SalesPersonProductID);
          SalesPeopleProduct frontendSalesPersonProduct = SalesPeopleProductList.GetSalesPeopleProductList().GetItem(SalesPersonProductID);
          if (frontendSalesPersonProduct.SalesPersonID == itemSalesPersonProduct.SalesPersonID)
          {
            return false;
          }
        }
      }
      return true;
    }

    [WebCallable]
    public static AppointmentList SaveAppointment(Appointment Appointment)
    {
      if (checkSalesPersonSchedule(Appointment.SalesPersonProductID, Appointment.StartDate, Appointment.EndDate))
      {
        SaveResult result = new SaveResult(Appointment.TrySave(typeof(AppointmentList)));
      }
      //Email client if appointment is new
      //Refresh the necessary common data lists
      return AppointmentList.GetAppointmentList();
    }

    [WebCallable]
    public static AppointmentList ConfirmAppointment(int AppointmentID)
    {
      Appointment Appointment = AppointmentList.GetAppointmentList().GetItem(AppointmentID);
      Appointment.AppointmentStatusID = 1;
      SaveResult result = new SaveResult(Appointment.TrySave(typeof(AppointmentList)));
      //Refresh the necessary common data lists
      return AppointmentList.GetAppointmentList();
    }

    [WebCallable]
    public static AppointmentList CancelAppointment(int AppointmentID)
    {
      Appointment Appointment = AppointmentList.GetAppointmentList().GetItem(AppointmentID);
      Appointment.AppointmentStatusID = 2;
      SaveResult result = new SaveResult(Appointment.TrySave(typeof(AppointmentList)));
      //Send an email to the client or admin
      //Refresh the necessary common data lists
      return AppointmentList.GetAppointmentList();
    }

    [WebCallable]
    public static AppointmentList RescheduleAppointment(int AppointmentID)
    {
      Appointment Appointment = AppointmentList.GetAppointmentList().GetItem(AppointmentID);
      Appointment.AppointmentStatusID = 3;
      SaveResult result = new SaveResult(Appointment.TrySave(typeof(AppointmentList)));
      //Send an email to the client or admin
      return AppointmentList.GetAppointmentList();
    }
  }
}