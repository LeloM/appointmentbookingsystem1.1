﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Appointments.aspx.cs" Inherits="OETWeb.Appointments.Appointments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <%
    using (var h = Helpers)
    {
      var toolbar = h.Toolbar();
      {
        toolbar.Helpers.HTML().Heading2("Appointments");

        var FilterDivision = toolbar.Helpers.DivC("row col-md-12");
        {
          var FilterDivision1 = FilterDivision.Helpers.DivC("row col-md-6");

          var FilterDivision2 = toolbar.Helpers.DivC("row col-md-6");
          {
            //if (Singular.Security.Security.HasAccess("Appointments", "Create new appointment"))
            //{
            var createAppointmentButton = FilterDivision2.Helpers.Button("Create Appointment", Singular.Web.ButtonMainStyle.Primary, Singular.Web.ButtonSize.Normal, Singular.Web.FontAwesomeIcon.plus);
            createAppointmentButton.AddClass("custom_btnMd");
            createAppointmentButton.Style.Margin("20px");
            createAppointmentButton.AddBinding(Singular.Web.KnockoutBindingString.click, "EditAppointment()");
            //}
          }
        }
      }

      h.MessageHolder();

      var panel = h.Div();
      {
        panel.AddClass("Panel");
        panel.Style.Margin("10px");

        var grid = h.TableFor<OETLib.Appointments.Appointment>(c => c.AppointmentList, false, false);
        {
          grid.AddClass("table-responsive table table-bordered");
          grid.Style.Margin("10px");

          var firstGridRow = grid.FirstRow;
          {
            firstGridRow.AddClass("table-responsive table table-bordered");
            firstGridRow.AddReadOnlyColumn(c => c.SalesPersonProductID);
            firstGridRow.AddReadOnlyColumn(c => c.ClientID);
            firstGridRow.AddReadOnlyColumn(c => c.AppointmentStatusID);
            firstGridRow.AddReadOnlyColumn(c => c.StartDate);
            firstGridRow.AddReadOnlyColumn(c => c.EndDate);
            firstGridRow.AddReadOnlyColumn(c => c.CreatedDate);
            firstGridRow.AddReadOnlyColumn(c => c.CreatedBy);

            if (Singular.Security.Security.HasAccess("Appointments", "Edit Appointment"))
            {
              var EditColumn = firstGridRow.AddColumn();
              EditColumn.Style.Width = "150";
              EditColumn.Style.TextAlign = Singular.Web.TextAlign.center;

              var DeleteColumn = firstGridRow.AddColumn();
              DeleteColumn.Style.Width = "150";
              DeleteColumn.Style.TextAlign = Singular.Web.TextAlign.center;

              var EditButton = EditColumn.Helpers.Button("Edit", Singular.Web.ButtonMainStyle.Primary, Singular.Web.ButtonSize.Normal, Singular.Web.FontAwesomeIcon.edit);
              EditButton.AddClass("custom_Bbtn_Md");
              EditButton.AddBinding(Singular.Web.KnockoutBindingString.click, "EditAppointment($data)");
              EditButton.Tooltip = "Edit Appointment";

              var DeleteButton = DeleteColumn.Helpers.Button("Delete", Singular.Web.ButtonMainStyle.NoStyle, Singular.Web.ButtonSize.Normal, Singular.Web.FontAwesomeIcon.trash);
              DeleteButton.AddClass("SmallDeleteButton");
              DeleteButton.AddBinding(Singular.Web.KnockoutBindingString.click, "DeleteAppointment($data)");
              DeleteButton.Tooltip = "Delete Appointment";
            }

            //if (Singular.Security.Security.HasAccess("Appointments", "Manage Appointment"))
            //{

            var ConfirmAppointmentColumn = firstGridRow.AddColumn();
            ConfirmAppointmentColumn.Style.Width = "150";
            ConfirmAppointmentColumn.Style.TextAlign = Singular.Web.TextAlign.center;

            var RescheduleAppointmentColumn = firstGridRow.AddColumn();
            RescheduleAppointmentColumn.Style.Width = "150";
            RescheduleAppointmentColumn.Style.TextAlign = Singular.Web.TextAlign.center;

            var DeleteAppointmentColumn = firstGridRow.AddColumn();
            DeleteAppointmentColumn.Style.Width = "150";
            DeleteAppointmentColumn.Style.TextAlign = Singular.Web.TextAlign.center;

            var ConfirmButton = ConfirmAppointmentColumn.Helpers.Button("Confirm", Singular.Web.ButtonMainStyle.Primary, Singular.Web.ButtonSize.Normal, Singular.Web.FontAwesomeIcon.check);
            ConfirmButton.AddClass("custom_Bbtn_Md");
            ConfirmButton.AddBinding(Singular.Web.KnockoutBindingString.click, "ConfirmAppointment($data)");
            ConfirmButton.Tooltip = "Confirm Appointment";

            var RescheduleButton = RescheduleAppointmentColumn.Helpers.Button("Reschedule", Singular.Web.ButtonMainStyle.Primary, Singular.Web.ButtonSize.Normal, Singular.Web.FontAwesomeIcon.refresh);
            RescheduleButton.AddClass("custom_Bbtn_Md");
            RescheduleButton.AddBinding(Singular.Web.KnockoutBindingString.click, "RescheduleAppointment($data)");
            RescheduleButton.Tooltip = "Reschedule Appointment";

            var CancelButton = DeleteAppointmentColumn.Helpers.Button("Cancel Appointment", Singular.Web.ButtonMainStyle.NoStyle, Singular.Web.ButtonSize.Normal, Singular.Web.FontAwesomeIcon.trash);
            CancelButton.AddClass("SmallDeleteButton");
            CancelButton.AddBinding(Singular.Web.KnockoutBindingString.click, "DeleteAppointment($data)");
            CancelButton.Tooltip = "Cancel Appointment";
            //}
          }
        }
      }


      var EditDialogBox = h.Dialog(
        c => c.EditingAppointment != null,
        c => "Create/Edit Appointment",
        "CancelEdit");
      {
        EditDialogBox.Style.Width = "600";
        var EditDialogBoxContent = EditDialogBox.Helpers.With<OETLib.Appointments.Appointment>(c => c.EditingAppointment);
        EditDialogBoxContent.Helpers.EditorRowFor(c => c.SalesPersonProductID);
        EditDialogBoxContent.Helpers.EditorRowFor(c => c.ClientID);
        var StartDate = EditDialogBoxContent.Helpers.EditorRowFor(c => c.StartDate);
        {
          var StartTime = StartDate.Helpers.TimeEditorFor(c => c.StartDate);
        }

        var EndDate = EditDialogBoxContent.Helpers.EditorRowFor(c => c.EndDate);
        {
          var EndTime = EndDate.Helpers.TimeEditorFor(c => c.EndDate);
        }

        var Notes = EditDialogBoxContent.Helpers.EditorRowFor(c => c.Note);
        {
          var Width = Notes.Editor.Style.Width = "500px";
          var Height = Notes.Editor.Style.Height = "200px";
        }

        var SaveButton = EditDialogBoxContent.Helpers.Button(Singular.Web.DefinedButtonType.Save);
        {
          SaveButton.PostBackType = Singular.Web.PostBackType.None;
          SaveButton.AddBinding(Singular.Web.KnockoutBindingString.enable, c => c.IsNew && c.IsValid);
          SaveButton.AddBinding(Singular.Web.KnockoutBindingString.click, "SaveAppointment()");
        }
      }
      //EditDialogBox.AddConfirmationButtons("Save", "SaveAppointment()", "Cancel");
    }
 %>

  <script type="text/javascript">
    Singular.OnPageLoad(function () {
      toggleMaintenanceSubMenu();
      $("maintenance2").addClass("activeSub");
      $("#Undo").remove();
    });

    var EditAppointment = function (Appointment) {
      if (Appointment) {
        ViewModel.CallServerMethod('GetAppointment', { AppointmentID: Appointment.AppointmentID(), ShowLoadingBar: true }, function (result) {
          if (result.Success) {
            ViewModel.EditingAppointment.Set(result.Data);
          }
        });
      } else {
        ViewModel.EditingAppointment.Set();
      }
    }

    var ConfirmAppointment = function (Appointment) {
      Singular.ShowMessageQuestion('Confirm', 'Are you sure you want to confirm this appointment?', function () {
        ViewModel.CallServerMethod('ConfirmAppointment', { AppointmentID: Appointment.AppointmentID(), ShowLoadingBar: true }, function (result) {
          if (result.Success) {
            ViewModel.AppointmentList.Set(result.Data);
            Singular.AddMessage(3, 'Appointment confirmed.').Fade(2000);
          } else {
            alert(result.ErrorText);
          }
        });
      });
    }

        var RescheduleAppointment = function (Appointment) {
      Singular.ShowMessageQuestion('Reschedule', 'Are you sure you want to reschedule this appointment?', function () {
        ViewModel.CallServerMethod('RescheduleAppointment', { AppointmentID: Appointment.AppointmentID(), ShowLoadingBar: true }, function (result) {
          if (result.Success) {
            ViewModel.AppointmentList.Set(result.Data);
            Singular.AddMessage(3, 'Appointment confirmed.').Fade(2000);
          } else {
            alert(result.ErrorText);
          }
        });
      });
    }

    var DeleteAppointment = function (Appointment) {
      Singular.ShowMessageQuestion('Delete', 'Are you sure you want to cancel this appointment?', function () {
        ViewModel.CallServerMethod('CancelAppointment', { AppointmentID: Appointment.AppointmentID(), ShowLoadingBar: true }, function (result) {
          if (result.Success) {
            ViewModel.AppointmentList.Set(result.Data);
            Singular.AddMessage(3, 'Appointment cancelled.').Fade(2000);
          } else {
            alert(result.ErrorText);
          }
        });
      });
    } 

    var SaveAppointment = function () {
      var jsonAppointment = ViewModel.EditingAppointment.Serialise();

      ViewModel.CallServerMethod('SaveAppointment', { Appointment: jsonAppointment, ShowLoadingBar: true }, function (result) {
        if (result.Success) {
          ViewModel.EditingAppointment.Clear();
          ViewModel.AppointmentList.Set(result.Data);
          Singular.AddMessage(3, 'Saved', 'Appointment has been successfully saved.').Fade(2000);
        } else {
          Singular.AddMessage(3, 'Appointment', 'Your booking clashes with another booking.').Fade(2000);
        }
      });
    }

    var CancelEdit = function () {
      ViewModel.EditingAppointment.Clear();
    }
  </script>
</asp:Content>
