﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="OETWeb.Account.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
	<link href="../Styles/dashboard.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

	<h1>Welcome to the Appointment Booking System!
	</h1>


	<%if (OETLib.Security.OETWebSecurity.HasAuthenticatedUser())
		{ %>
	<div>
		<h3>
      How can we help you today?
		</h3>
	</div>
	<%}%>

	<%if (!OETLib.Security.OETWebSecurity.HasAuthenticatedUser())
		{%>
	<div>
    <h3>
      Thank you for using the Appointment Booking System and have a beautiful day!
    </h3>
	</div>
	<%}%>
</asp:Content>
