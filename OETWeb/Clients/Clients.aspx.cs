﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Singular.Web.MaintenanceHelpers;
using Singular.Security;
using System.ComponentModel;
using Singular.Web.Data;
using OETLib.Security;
using OETLib;
using OETLib.Clients;
using Singular.Web;
using OETWeb;

namespace OETWeb.Clients
{
  public partial class Clients : OETPageBase<ClientsVM>
  {
  }

  public class ClientsVM : OETStatelessViewModel<ClientsVM>
  {
    public Client Clients { get; set; }

    public ClientList ClientList { get; set; }

    public Client EditingClient { get; set; }

    protected override void Setup()
    {
      base.Setup();
      CommonData.Lists.Refresh(typeof(OETLib.Maintenance.ROClientList));
      this.ClientList = ClientList.GetClientList();
      this.ValidationDisplayMode = ValidationDisplayMode.Controls | ValidationDisplayMode.SubmitMessage;
      this.EditingClient = null;
    }

    [WebCallable]
    public static Client GetClient(int ClientID)
    {
      return ClientList.GetClientList().GetItem(ClientID);
    }

    [WebCallable]
    public static ClientList SaveClient(Client client)
    {
      SaveResult result = new SaveResult(client.TrySave(typeof(ClientList)));
      //Refresh the necessary common data lists
      return ClientList.GetClientList();
    }

    [WebCallable]
    public static ClientList DeleteClient(int ClientID)
    {
      Client client = ClientList.GetClientList().GetItem(ClientID);
      client.isDeleted = true;
      SaveResult result = new SaveResult(client.TrySave(typeof(ClientList)));
      //Refresh the necessary common data lists
      return ClientList.GetClientList();
    }
  }
}