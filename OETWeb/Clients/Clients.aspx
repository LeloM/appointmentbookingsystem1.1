﻿<%@ Page Title="Clients" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="false" CodeBehind="Clients.aspx.cs" Inherits="OETWeb.Clients.Clients" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <%
    using (var h = Helpers)
    {
      var toolbar = h.Toolbar();
      {
        toolbar.Helpers.HTML().Heading2("Clients");

        var FilterDivision = toolbar.Helpers.DivC("row col-md-12");
        {
          var FilterDivision1 = FilterDivision.Helpers.DivC("row col-md 6");

          var FilterDivision2 = toolbar.Helpers.DivC("row col-md-6");
          {
            //if (Singular.Security.Security.HasAccess("Clients", "Add new client"))
            //{
            var addClientButton = FilterDivision2.Helpers.Button("Add Client", Singular.Web.ButtonMainStyle.Primary, Singular.Web.ButtonSize.Normal, Singular.Web.FontAwesomeIcon.plus);
            addClientButton.AddClass("custom_btnMd");
            addClientButton.Style.Margin("20px");
            addClientButton.AddBinding(Singular.Web.KnockoutBindingString.click, "EditClient()");
            //}
          }
        }
      }

      h.MessageHolder();

      var panel = h.Div();
      {
        panel.AddClass("Panel");
        panel.Style.Margin("10px");

        var grid = h.TableFor<OETLib.Clients.Client>(c => c.ClientList, false, false);
        {
          grid.AddClass("table-responsive table table-bordered");
          grid.Style.Margin("10px");

          var firstGridRow = grid.FirstRow;
          {
            firstGridRow.AddClass("table-responsive table table-bordered");
            firstGridRow.AddReadOnlyColumn(c => c.FirstName);
            firstGridRow.AddReadOnlyColumn(c => c.LastName);
            firstGridRow.AddReadOnlyColumn(c => c.ContactNumber);
            firstGridRow.AddReadOnlyColumn(c => c.AlternativeContactNumber);
            firstGridRow.AddReadOnlyColumn(c => c.EmailAddress);
            firstGridRow.AddReadOnlyColumn(c => c.CreatedBy);
            firstGridRow.AddReadOnlyColumn(c => c.CreatedDate);

            var EditColumn = firstGridRow.AddColumn();
            EditColumn.Style.Width = "150";
            EditColumn.Style.TextAlign = Singular.Web.TextAlign.center;

            var DeleteColumn = firstGridRow.AddColumn();
            DeleteColumn.Style.Width = "150";
            DeleteColumn.Style.TextAlign = Singular.Web.TextAlign.center;

            //if (Singular.Security.Security.HasAccess("Clients", "Edit clients"))
            //{
            var EditButton = EditColumn.Helpers.Button("Edit", Singular.Web.ButtonMainStyle.NoStyle, Singular.Web.ButtonSize.Normal, Singular.Web.FontAwesomeIcon.edit);
            EditButton.AddClass("custom_Bbtn_Md");
            EditButton.AddBinding(Singular.Web.KnockoutBindingString.click, "EditClient($data)");
            EditButton.Tooltip = "Edit Client";

            var DeleteButton = DeleteColumn.Helpers.Button("Delete", Singular.Web.ButtonMainStyle.NoStyle, Singular.Web.ButtonSize.Normal, Singular.Web.FontAwesomeIcon.trash);
            DeleteButton.AddClass("SmallDeleteButton");
            DeleteButton.AddBinding(Singular.Web.KnockoutBindingString.click, "DeleteClient($data)");
            DeleteButton.Tooltip = "Delete Client";
            //}
          }
        }
      }

      var dialogBox = h.Dialog(
        c => c.EditingClient != null,
        c => "Client Details",
        "CancelEdit"
        );

      {
        dialogBox.Style.Width = "600";
        var dialogBoxContent = dialogBox.Helpers.With<OETLib.Clients.Client>(c => c.EditingClient);
        dialogBoxContent.Helpers.EditorRowFor(c => c.FirstName);
        dialogBoxContent.Helpers.EditorRowFor(c => c.LastName);
        dialogBoxContent.Helpers.EditorRowFor(c => c.ContactNumber);
        dialogBoxContent.Helpers.EditorRowFor(c => c.AlternativeContactNumber);
        dialogBoxContent.Helpers.EditorRowFor(c => c.EmailAddress);

      }

      dialogBox.AddConfirmationButtons("Save", "SaveClient()", "Cancel");
    }


  %>

  <script type="text/javascript">
    Singular.OnPageLoad(function () {
      toggleMaintenanceSubMenu();
      $("maintenance2").addClass("activeSub");
      $("#Undo").remove();
    });

    var EditClient = function (Client) {
      if (Client) {
        ViewModel.CallServerMethod('GetClient', { ClientID: Client.ClientID(), ShowLoadingBar: true }, function (result) {
          if (result.Success) {
            ViewModel.EditingClient.Set(result.Data);
          }
        });
      } else {
        ViewModel.EditingClient.Set();
      }
    }

    var CancelEdit = function () {
      ViewModel.EditingClient.Clear();
    }

    var SaveClient = function () {
        var jsonClient = ViewModel.EditingClient.Serialise();

        ViewModel.CallServerMethod('SaveClient', { client: jsonClient, ShowLoadingBar: true }, function (result) {
          if (result.Success) {
            ViewModel.EditingClient.Clear();
            ViewModel.ClientList.Set(result.Data);
            Singular.AddMessage(3, 'Saved', 'Client has been saved successfully').Fade(2000);
          }
        });
    }

    var DeleteClient = function (Client) {
      Singular.ShowMessageQuestion('Delete', 'Are you sure you want to permanently delete this client?', function () {
        ViewModel.CallServerMethod('DeleteClient', { ClientID: Client.ClientID(), ShowLoadingBar: true }, function (result) {
          if (result.Success) {
            ViewModel.ClientList.Set(result.Data);
            Singular.AddMessage(3, 'Delete', 'Client has been deleted successfully.').Fade(2000);
          } else {
            alert(result.ErrorText);
          }
        });
      });
    }


  </script>
</asp:Content>
