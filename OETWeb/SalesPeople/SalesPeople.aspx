﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SalesPeople.aspx.cs" Inherits="OETWeb.SalesPeople.SalesPeople" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <%
    using (var h = Helpers)
    {
      var toolbar = h.Toolbar();
      {
        toolbar.Helpers.HTML().Heading2("Sales People");

        var FilterDivision = toolbar.Helpers.DivC("row col-md-12");
        {
          var FilterDivision1 = FilterDivision.Helpers.DivC("row col-md-6");

          var FilterDivision2 = toolbar.Helpers.DivC("row col-md-6");
          {
            //if (Singular.Security.Security.HasAccess("Sales People", "Add new sales person"))
            //{
            var addSalesPersonButton = FilterDivision2.Helpers.Button("Add Sales Person", Singular.Web.ButtonMainStyle.Primary, Singular.Web.ButtonSize.Normal, Singular.Web.FontAwesomeIcon.plus);
            addSalesPersonButton.AddClass("custom_btnMd");
            addSalesPersonButton.Style.Margin("20px");
            addSalesPersonButton.AddBinding(Singular.Web.KnockoutBindingString.click, "EditSalesPerson()");
            //}
          }
        }
      }

      h.MessageHolder();

      var panel = h.Div();
      {
        panel.AddClass("Panel");
        panel.Style.Margin("10px");

        var grid = h.TableFor<OETLib.SalesPeople.SalesPeople>(c => c.SalesPeopleList, false, false);
        {
          grid.AddClass("table-responsive table table-bordered");
          grid.Style.Margin("10px");

          var firstGridRow = grid.FirstRow;
          {
            firstGridRow.AddClass("table-responsive table table-bordered");
            firstGridRow.AddReadOnlyColumn(c => c.FirstName);
            firstGridRow.AddReadOnlyColumn(c => c.LastName);
            firstGridRow.AddReadOnlyColumn(c => c.ContactNumber);
            firstGridRow.AddReadOnlyColumn(c => c.AlternativeContactNumber);
            firstGridRow.AddReadOnlyColumn(c => c.EmailAddress);
            firstGridRow.AddReadOnlyColumn(c => c.CreatedBy);
            firstGridRow.AddReadOnlyColumn(c => c.CreatedDate);

            var EditColumn = firstGridRow.AddColumn();
            EditColumn.Style.Width = "150";
            EditColumn.Style.TextAlign = Singular.Web.TextAlign.center;

            var DeleteColumn = firstGridRow.AddColumn();
            DeleteColumn.Style.Width = "150";
            DeleteColumn.Style.TextAlign = Singular.Web.TextAlign.center;

            //if (Singular.Security.Security.HasAccess("Sales People", "Edit sales person"))
            //{
            var EditButton = EditColumn.Helpers.Button("Edit", Singular.Web.ButtonMainStyle.Primary, Singular.Web.ButtonSize.Normal, Singular.Web.FontAwesomeIcon.edit);
            EditButton.AddClass("custom_Bbtn_Md");
            EditButton.AddBinding(Singular.Web.KnockoutBindingString.click, "EditSalesPerson($data)");
            EditButton.Tooltip = "Edit Sales Person";

            var DeleteButton = DeleteColumn.Helpers.Button("Delete", Singular.Web.ButtonMainStyle.NoStyle, Singular.Web.ButtonSize.Normal, Singular.Web.FontAwesomeIcon.trash);
            DeleteButton.AddClass("SmallDeleteButton");
            DeleteButton.AddBinding(Singular.Web.KnockoutBindingString.click, "DeleteSalesPerson($data)");
            DeleteButton.Tooltip = "Delete Sales Person";
            //}
          }
        }
      }

      var dialogBox = h.Dialog(
        c => c.EditingSalesPerson != null,
        c => "Editing Sales Person",
        "CancelEdit");

      {
        dialogBox.Style.Width = "600";
        var dialogBoxContent = dialogBox.Helpers.With<OETLib.SalesPeople.SalesPeople>(c => c.EditingSalesPerson);
        dialogBoxContent.Helpers.EditorRowFor(c => c.FirstName);
        dialogBoxContent.Helpers.EditorRowFor(c => c.LastName);
        dialogBoxContent.Helpers.EditorRowFor(c => c.ContactNumber);
        dialogBoxContent.Helpers.EditorRowFor(c => c.AlternativeContactNumber);
        dialogBoxContent.Helpers.EditorRowFor(c => c.EmailAddress);
      }
      dialogBox.AddConfirmationButtons("Save", "SaveSalesPerson()", "Cancel");
    }

  %>

  <script type="text/javascript">
    Singular.OnPageLoad(function () {
      toggleMaintenanceSubMenu();
      $("maintenance2").addClass("activeSub");
      $("#Undo").remove();
    });

    var EditSalesPerson = function (SalesPerson) {
      if (SalesPerson) {
        ViewModel.CallServerMethod('GetSalesPerson', { SalesPersonID: SalesPerson.SalesPersonID(), ShowLoadingBar: true }, function (result) {
          if (result.Success) {
            ViewModel.EditingSalesPerson.Set(result.Data);
          }
        });
      } else {
        ViewModel.EditingSalesPerson.Set();
      }
    }

    var CancelEdit = function () {
      ViewModel.EditingSalesPerson.Clear();
    }

    var SaveSalesPerson = function () {
      var jsonSalesPerson = ViewModel.EditingSalesPerson.Serialise();

      ViewModel.CallServerMethod('SaveSalesPerson', { salesPerson: jsonSalesPerson, ShowLoadingBar: true }, function (result) {
        if (result.Success) {
          ViewModel.EditingSalesPerson.Clear();
          ViewModel.SalesPeopleList.Set(result.Data);
          Singular.AddMessage(3, 'Saved', 'Sales Person has been saved successfully').Fade(2000);
        }
      });
    }

    var DeleteSalesPerson = function (SalesPerson) {
      Singular.ShowMessageQuestion('Delete', 'Are you sure you want to permanently delete this sales person?', function () {
        ViewModel.CallServerMethod('DeleteSalesPerson', { SalesPersonID: SalesPerson.SalesPersonID(), ShowLoadingBar: true }, function (result) {
          if (result.Success) {
            ViewModel.SalesPeopleList.Set(result.Data);
            Singular.AddMessage(3, 'Delete', 'Sales Person has been deleted successfully.').Fade(2000);
          } else {
            alert(result.ErrorText);
          }
        });
      });
    }
  </script>

</asp:Content>
