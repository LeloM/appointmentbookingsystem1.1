﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Singular.Web.MaintenanceHelpers;
using Singular.Security;
using System.ComponentModel;
using Singular.Web.Data;
using OETLib;
using OETLib.Security;
using OETLib.SalesPeople;
using Singular.Web;

namespace OETWeb.SalesPeople
{
  public partial class SalesPeople : OETPageBase<SalesPeopleVM>
  {
  }

  public class SalesPeopleVM : OETStatelessViewModel<SalesPeopleVM>
  {
    public OETLib.SalesPeople.SalesPeople SalesPeople { get; set; }

    public SalesPeopleList SalesPeopleList { get; set; }

    public OETLib.SalesPeople.SalesPeople EditingSalesPerson { get; set; }

    protected override void Setup()
    {
      base.Setup();
      CommonData.Lists.Refresh(typeof(OETLib.Maintenance.ROSalesPeopleList));
      this.SalesPeopleList = SalesPeopleList.GetSalesPeopleList();
      this.ValidationDisplayMode = ValidationDisplayMode.Controls | ValidationDisplayMode.SubmitMessage;
      this.EditingSalesPerson = null;
    }

    [WebCallable]
    public static OETLib.SalesPeople.SalesPeople GetSalesPerson(int SalesPersonID)
    {
      return SalesPeopleList.GetSalesPeopleList().GetItem(SalesPersonID);
    }

    [WebCallable]
    public static SalesPeopleList SaveSalesPerson(OETLib.SalesPeople.SalesPeople salesPerson)
    {
      SaveResult result = new SaveResult(salesPerson.TrySave(typeof(SalesPeopleList)));
      //Refresh the necessary common data lists
      return SalesPeopleList.GetSalesPeopleList();
    }

    [WebCallable]
    public static SalesPeopleList DeleteSalesPerson(int SalesPersonID)
    {
      OETLib.SalesPeople.SalesPeople salesPerson = SalesPeopleList.GetSalesPeopleList().GetItem(SalesPersonID);
      salesPerson.isDeleted = true;
      SaveResult result = new SaveResult(salesPerson.TrySave(typeof(SalesPeopleList)));
      //Refresh the necessary common data lists
      return SalesPeopleList.GetSalesPeopleList();
    }
  }
}