using System;
using OETLib.Maintenance;
using Singular.CommonData;

namespace OETLib
{
  // ToDo: OETCachedLists isn't recognised unless the class is moved
  // out of the CommonData class. Is this a problem?
  public class CommonData : CommonDataBase<OETLib.CommonData.OETCachedLists>
  {
    [Serializable]
    public class OETCachedLists : CommonDataBase<OETCachedLists>.CachedLists
    {
      public OETLib.Maintenance.ROClientList ReadOnlyClientList
      {
        get
        {
          return RegisterList<OETLib.Maintenance.ROClientList>(Misc.ContextType.Application, c => c.ReadOnlyClientList, () => { return OETLib.Maintenance.ROClientList.GetROClientList(); });
        }
      }

      public OETLib.Maintenance.ROSalesPeopleProductList ReadOnlySalesPeopleProductList
      {
        get
        {
          return RegisterList<OETLib.Maintenance.ROSalesPeopleProductList>(Misc.ContextType.Application, c => c.ReadOnlySalesPeopleProductList, () => { return OETLib.Maintenance.ROSalesPeopleProductList.GetROSalesPeopleProductList(); });
        }
      }

      public OETLib.Maintenance.ROUserList ReadOnlyUserList
      {
        get
        {
          return RegisterList<OETLib.Maintenance.ROUserList>(Misc.ContextType.Application, c => c.ReadOnlyUserList, () => { return OETLib.Maintenance.ROUserList.GetROUserList(); });
        }
      }

      public OETLib.Maintenance.ROAppointmentStatusList ReadOnlyAppointmentStatusList
      {
        get
        {
          return RegisterList<OETLib.Maintenance.ROAppointmentStatusList>(Misc.ContextType.Application, c => c.ReadOnlyAppointmentStatusList, () => { return OETLib.Maintenance.ROAppointmentStatusList.GetROAppointmentStatuList(); });
        }
      }
    }
  }

  public class Enums
  {
  }
}
