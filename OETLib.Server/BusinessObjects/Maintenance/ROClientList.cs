﻿// Generated 06 Sep 2021 14:02 - Singular Systems Object Generator Version 2.2.694
//<auto-generated/>
using System;
using Csla;
using Csla.Serialization;
using Csla.Data;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Singular;
using System.Data;
using System.Data.SqlClient;


namespace OETLib.Maintenance
{
  [Serializable]
  public class ROClientList
   : SingularReadOnlyListBase<ROClientList, ROClient>
  {
    #region " Business Methods "

    public ROClient GetItem(int ClientID)
    {
      foreach (ROClient child in this)
      {
        if (child.ClientID == ClientID)
        {
          return child;
        }
      }
      return null;
    }

    public override string ToString()
    {
      return "RO Clients";
    }

    #endregion

    #region " Data Access "

    [Serializable]
    public class Criteria
      : CriteriaBase<Criteria>
    {
      public Criteria()
      {
      }

    }

    public static ROClientList NewROClientList()
    {
      return new ROClientList();
    }

    public ROClientList()
    {
      // must have parameter-less constructor
    }

    public static ROClientList GetROClientList()
    {
      return DataPortal.Fetch<ROClientList>(new Criteria());
    }

    protected void Fetch(SafeDataReader sdr)
    {
      this.RaiseListChangedEvents = false;
      this.IsReadOnly = false;
      while (sdr.Read())
      {
        this.Add(ROClient.GetROClient(sdr));
      }
      this.IsReadOnly = true;
      this.RaiseListChangedEvents = true;
    }

    protected override void DataPortal_Fetch(Object criteria)
    {
      Criteria crit = (Criteria)criteria;
      using (SqlConnection cn = new SqlConnection(Singular.Settings.ConnectionString))
      {
        cn.Open();
        try
        {
          using (SqlCommand cm = cn.CreateCommand())
          {
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "GetProcs.getROClientList";
            using (SafeDataReader sdr = new SafeDataReader(cm.ExecuteReader()))
            {
              Fetch(sdr);
            }
          }
        }
        finally
        {
          cn.Close();
        }
      }
    }

    #endregion

  }

}